const mongoose = require('mongoose');
const Spreadsheet = mongoose.model('Spreadsheets');


module.exports = app => {

  app.get('/api/spreadsheets/',  async (req, res) => {
    const spreadSheets = await Spreadsheet.find({});
    res.send(spreadSheets);
  });

  app.get('/api/spreadsheets/:id', async (req, res) => {
    const id = req.params.id;

    console.log("REST: Request for spreadsheet data " + id)

    console.log(id);
    if (id === 'new') {
        newItem = Spreadsheet();
        newItem.save();
        return res.send(newItem);
    }

    if (id.startsWith("ATTACH_")) {
      attached_id = id.replace("ATTACH_", "");
      console.log(attached_id);
      
      // Check if we've attached this spreadsheet before
      existingSheet = await Spreadsheet.findOne({uuid: attached_id});
      if (existingSheet) { 
        console.log("EXISTING SHEET"); 
        res.send(existingSheet);
        return; }

      attachedItem = Spreadsheet({uuid:attached_id});
      attachedItem.attached = true;
      attachedItem.save();
      return res.send(attachedItem);
    }

    const spreadSheet = await Spreadsheet.findOne({uuid: id});
    res.send(spreadSheet);
  });

  app.post('/api/spreadsheets', async (req, res) => {
      console.log(req.body);
      const newSpreadSheet = Spreadsheet(req.body);
      newSpreadSheet.save();
      res.status(200).send("ok");
  });

  app.put('/api/spreadsheets/:id', async (req, res) => {
    const id = req.params.id;
    console.log(req.body);
    console.log("REST: Update spreadsheet data " + id)

    console.log(id);
    if (id === 'new') {
        newItem = Spreadsheet();
        newItem.save();
        return res.send(newItem);
    }
    
    const spreadSheet = await Spreadsheet.findOne({uuid: id});
    if (req.body.name) {
      spreadSheet.name = req.body.name
      await spreadSheet.save()
    }

    res.send(spreadSheet);
   });

};