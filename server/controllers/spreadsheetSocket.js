const mongoose = require('mongoose');
const Spreadsheet = mongoose.model('Spreadsheets');


// Controller to send / recevive commits from exchange via NuCypher microservice

module.exports = socket => {

  socket.on('initTable',  async(data) => {
    const id = data.tableId;
    if (id === 'new') {
      newItem = Spreadsheet();
      return socket.emit("@@GRID/RECEIVE_SUCCESS", JSON.stringify(newItem));
    }
    const spreadSheet = await Spreadsheet.findOne({uuid: id});
    socket.emit("@@GRID/RECEIVE_SUCCESS", spreadSheet);

  });

  socket.on('updateTable',  async(data) => {
    //console.log("NEW DATA RECEIVED", data);
    const id = data.tableId;
    const row = data.fromRow;
    const column = Object.entries(data.updated)[0][0];
    const value = Object.entries(data.updated)[0][1];

    updatedTable =  await Spreadsheet.findOne({uuid: id});
    await updatedTable.applyCommit({column, row, value});
   
    socket.emit("@@GRID/RECEIVE_SUCCESS", updatedTable);
  });

};