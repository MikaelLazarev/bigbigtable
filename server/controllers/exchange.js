const mongoose = require('mongoose');
const Spreadsheet = mongoose.model('Spreadsheets');
const redis = require('redis');


module.exports = (exchangeIO, socket) => {

  const redisSubToExchange = redis.createClient(6379, 'redis');
  const redisSubFromExchange = redis.createClient(6379, 'redis');
  const redisPublisher = redis.createClient(6379, 'redis');

  redisSubToExchange.subscribe("to_exchange_encrypted")
  redisSubToExchange.on("message", (channel, message) => {

    const data = JSON.parse(message);
    exchangeIO.emit("push_commit", data);
    //redisPublisher.publish("from_exchange", JSON.stringify(data));

  })

  exchangeIO.on('pull_commit',  async (data) => {

    console.log('PULL COMMIT RECEIVED', data);
    const { uuid, commitNumber } = data;
    let spreadSheet = await Spreadsheet.findOne({uuid});
    if (!spreadSheet) {
        spreadSheet = new Spreadsheet({uuid, attached: true})
        await spreadSheet.save()
         }
    if (spreadSheet.attached) redisPublisher.publish("from_exchange", JSON.stringify(data));

  });

  redisSubFromExchange.subscribe("from_exchange_decrypted")
  redisSubFromExchange.on("message", async (channel, message) => {
    console.log("DECRYPTED PULL COMMIT RECEIVED: ", channel, message)

    const data = JSON.parse(message);
    const { uuid, row, column, commitNumber, value } = data;

    let spreadSheet = await Spreadsheet.findOne({uuid});
    if (!spreadSheet) {
        // CREATES NEW SPREADSHEET
        spreadSheet = new Spreadsheet({uuid, attached: true})
        await spreadSheet.save()
         }

    if (spreadSheet.attached) {

      await spreadSheet.applyCommit({column, row, value});
      // Broadcast update to all client
      socket.broadcast.emit("@@GRID/RECEIVE_SUCCESS", spreadSheet);

      console.log("Commit " + commitNumber + " was applied");
    }

  } )

};