const mongoose = require('mongoose');
const uuidv4 = require('uuid/v4');
const redis = require('redis');

const redisPub = redis.createClient(6379, 'redis');

const { Schema } = mongoose;

const cellSchema = new Schema ({
    column: String,
    cellType: Number,
    valueString: String,
    valueNumber: Number,
    isConnected: String
});

const rowSchema = new Schema({
    row: Number,
    cells: [cellSchema]
});



const tableSchema = new Schema ({
    columns: [{key: String, name: String, editable: Boolean}],
    rows: [rowSchema],
    updated: { type: Date, default: Date.now },
});

tableSchema.methods.initalize = function(readOnly) {

    console.log("TABLE Init");
    const defaultRowCells = [];
    const Cell = mongoose.model("Cell", cellSchema);
    const Row = mongoose.model("Row", rowSchema);

    for(let i=0; i<26; i++) {
        const columnLetter = String.fromCharCode("A".charCodeAt(0) + i)
        this.columns.push({key: i, name: columnLetter, editable: !readOnly})
        //defaultRowCells.push(new Cell({column: columnHash }));
    }
    for(let i=1; i<=40; i++) {
        this.rows.push({});
        }

}

const spreadsheetSchema = new Schema ({

    name: {type: String, default: "New spreadsheet"},
    uuid: {type: String, default: uuidv4, unique: true},
    updated: { type: Date, default: Date.now },
    attached: { type: Boolean, default: false},
    lastCommit: { type: Number, default: 0 },
    table: tableSchema

});

spreadsheetSchema.pre('save', function(next) {

    if (this.table === undefined) {
        const Table = mongoose.model('Table', tableSchema)
        this.table = new Table({});
        this.table.initalize(this.attached);
    }
    next();
})

spreadsheetSchema.virtual('id').get(function(){
    return this.uuid;
});

// Ensure virtual fields are serialised.
spreadsheetSchema.set('toJSON', {
    virtuals: true
});

spreadsheetSchema.methods.applyCommit = async function(newCommit) {
    //try{
        const Cell = mongoose.model('Cells');


        const {column, row, value, commitNumber} = newCommit;
        const cells = this.table.rows[row].cells;
        if (cells[column]) { cells[column].valueString= value;
        } else { cells[column] = new Cell({column: column, valueString: value})}
       
        await this.save();
        if (!this.attached) {
            this.lastCommit++;
            console.log("REDIS: Sending commit " + this.lastCommit);
            redisPub.publish("to_exchange", JSON.stringify({uuid: this.uuid, commitNumber: this.lastCommit, column, row, value}));

            this.save();
            //const history = History({uuid: this.uuid, column, row, value})
            //await history.save();
        } else {
            this.lastCommit = Math.max(this.last_commit, commitNumber)
        }
        
/*
    } catch {
        console.log("Error was occured");
    }
*/
}


mongoose.model('Spreadsheets', spreadsheetSchema);
mongoose.model('Cells', cellSchema);