const mongoose = require('mongoose');
const uuidv4 = require('uuid/v4');
const { Schema } = mongoose;


const historySchema = new Schema ({

    uuid: {type: String },
    row: {type: Number},
    column: {type: Number},
    value: {type: String},
    updated: { type: Date, default: Date.now },
    syncronized: { type: Boolean, default: false }
});

historySchema.virtual('id').get(function(){
    return this.uuid;
});

// Ensure virtual fields are serialised.
historySchema.set('toJSON', {
    virtuals: true
});

mongoose.model('History', historySchema);
