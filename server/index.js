const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const socketIOClient = require('socket.io-client');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const helmet = require('helmet');


// Imoprt models
require('./models/spreadsheet');
require('./models/local_history');


// Connects to Mongoose
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_DB_URI, (err => {
  if (err) { console.log("Mongo connection problem" + err); throw err; }
}));

const app = express();
app.use(helmet())
app.use(bodyParser.json());

// Register REST Controllers
require('./controllers/spreadsheetREST')(app);

app.get("/", (req, res) => {
    return res.send("Server works");
})


if (process.env.NODE_ENV === 'production') {
  // Express will serve up production assets
  // like our main.js file, or main.css file!
  app.use(express.static('../client/build'));

  // Express will serve up the index.html file
  // if it doesn't recognize the route
  const path = require('path');
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'client', 'build', 'index.html'));
  });
}

const PORT = process.env.EXPRESS_PORT || 5000;

// Register socket.io
const server = http.createServer(app);
const io = socketIO(server);

// Register socket.io Client
const exchangeClient = socketIOClient("http://exchange:6000")

// Start listening
server.listen(PORT, () => console.log(`Express & socket.io server listening on ${PORT}`));


// Register socket.io endrpoints
io.on('connection', socket => {
  console.log("New connection is set up");
  socket.on('disconnect', () => console.log("Disconnected"));
  // Adding SocketIO controllers for spreadsheets
  require("./controllers/spreadsheetSocket")(socket);
  require('./controllers/exchange')(exchangeClient, socket);
});

