import React from 'react';
import ReactDOM from 'react-dom';
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'
import { ReactReduxContext } from 'react-redux'

import App from './containers/App';
import configureStore from './store'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';

const history = createHistory();

export const store = configureStore(history);

console.log("PROCESS", process.env);

ReactDOM.render((
		<Provider store={store}>
	      	<ConnectedRouter history={history}>
	        		<App />
			</ConnectedRouter>
		</Provider>

  ), document.getElementById('root'));
