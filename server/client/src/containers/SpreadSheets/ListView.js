import React, { Component } from "react";
import {Col, Container, Media, Row, ListGroup} from "react-bootstrap";
import {Link} from "react-router-dom";

import ListDataLoader from "../../components/DataLoaders/DataLoaderList"
import PageHeader from "../../components/PageHeader";
import ProjectCard from "./ProjectCard"
import InfiniteScroll from "react-infinite-scroller";
import {api, resource} from "./_config";


class ListView extends Component {

    render() {

        const items = Object.values(this.props.data)
            .sort((a,b) => {
                if (a.date > b.date) return -1;
                return 1;
            })
            .map(item => {
                return <ProjectCard
                            key={"EC" + item.id}
                            {...item}/>
                        });

        return (
            <div>
            <PageHeader
                header = "Spreedsheets"
                subHeader = "Your spreadsheets"
                resource={resource}
                searchBar
            />
            <Container fluid style={{paddingLeft: 40, paddingRight: 40, paddingTop: 20}}>
            <Row>
                <Col sm={2} style={{padding: "0 0 0 0"}}>
                <ListGroup style={{padding: "0 0 0 0"}}>
                    <ListGroup.Item>
                        <Link to="/spreadsheets/new">New spreadsheet</Link>
                    </ListGroup.Item>
                    <ListGroup.Item>My spreadsheets</ListGroup.Item>
                    <ListGroup.Item>Shared spreadsheets</ListGroup.Item>
                </ListGroup>
                </Col>
                <Col sm={10}>
                    <InfiniteScroll
                        page={1}
                        loadMore={this.props.loadMore}
                        hasMore = {this.props.hasMore}

                        >
                         <Row>
                            {items}
                        </Row>
                    </InfiniteScroll>
                </Col>
            </Row>

            </Container>

            </div>
        );

        }

};



export default ListDataLoader(ListView, api, resource);