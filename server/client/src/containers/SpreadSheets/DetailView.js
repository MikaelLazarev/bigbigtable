import React from "react";
import {Col, Row, Tab} from "react-bootstrap";
import {Redirect, Link} from "react-router-dom";

import PageHeader from "../../components/PageHeader";
import DetailDataLoaderWrapper from "../../components/DataLoaders/DataLoaderDetail"

import {api, resource} from "./_config";
import TabBar from "../../components/TabBar/TabBar";
import Grid from "../Tables/Grid"
import icon from "./icon.png"


function DetailView(props) {

    if (props.id === 'new') {
        if (props.data.id !== 'new') return <Redirect to={"/spreadsheets/" + props.data.id + "/"}/>
        return "Creating database";
    }

    if (props.id.startsWith("ATTACH_")) {
        if (props.data.id !== undefined) return <Redirect to={"/spreadsheets/" + props.data.id + "/" }/>
        return "Attaching new spreadsheet, please wait...";
    }

    if (props.data === undefined) return "Loading";
    const attachedLink = (props.data.attached) ? "READ ONLY PERMISSION" : <>Shareable link: <Link to={"/spreadsheets/ATTACH_" + props.data.id + "_ECHO"}>{ props.data.id + "_ECHO" } </Link></>
    const name = <> {props.data.name} <Link to={'edit/'}> [Edit]</Link> </>
    return (
        <>
            <PageHeader
                header={ name }
                title = {props.data.name}
                subHeader={attachedLink}
                icon={icon}
            />

            <TabBar id={props.id} resource={resource} tab={props.match.params.tab} history={props.history} tab='table'>

                <Tab.Pane eventKey="table" label={"Table"}>
                        <Grid id={props.id}/>
                </Tab.Pane>
                 <Tab.Pane eventKey="permissions" label={"Permissions"}>
                     Not yet developed

                </Tab.Pane>

            </TabBar>
        </>
    );

};

export default DetailDataLoaderWrapper(DetailView, api, resource);