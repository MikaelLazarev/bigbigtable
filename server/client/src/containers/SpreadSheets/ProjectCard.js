import React from "react";
import {Card, Col } from "react-bootstrap";
import {Link} from "react-router-dom";

import PropTypes from 'prop-types';
import icon from "./icon.png"

export const ProjectCard = (props) => {

    return <Col sm={6} md={6} lg={3} style={{marginBottom: 20}}>
                <Card style={{minHeight: 150}}>
                    <Card.Img top width="100%" src={ icon } alt="Spreadsheet icon" />
                    <Card.Body>
                      <Card.Title>
                          <Link to={"/spreadsheets/" + props.id + "/"}>
                          { props.name }
                          </Link>
                      </Card.Title>
                      <Card.Text>
                        <small className="text-muted">Last updated 3 mins ago</small>
                      </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
};


ProjectCard.propTypes = {
    icon: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    date: PropTypes.string.isRequired
};


export default ProjectCard;