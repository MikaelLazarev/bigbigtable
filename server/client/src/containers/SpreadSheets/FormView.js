import React, { Component } from "react";
import { Container } from "react-bootstrap";
import * as Yup from 'yup';

import PageHeader from "../../components/PageHeader";
import DetailDataLoaderWrapper from "../../components/DataLoaders/DataLoaderDetail";
import FormikForm from "../../components/Forms/FormikForm";

import {api, resource} from "./_config";

class FormView extends Component {

    render() {

        const fieldsList = {
            name: {
                  title: 'Name',
                  sm: 12,
                  validation: Yup.string().required('Required')
            },

            author: {
                  title: 'Author',
                  sm: 12,
                  validation: Yup.string()
            },
           
        };

        const backLink = "/spreadsheets/" + this.props.id;
        return <>
                <PageHeader
                    header = { `Spreadsheet ${this.props.data.name}` }
                    subHeader =  { this.props.data.comment }
                />
                <Container fluid style={{ paddingLeft: 40, paddingRight: 40}}>
                    <FormikForm fieldList = {fieldsList}
                                onSubmit = {this.onSubmit}
                                initialValues = {this.props.data}
                                onSuccessLink = {backLink}
                                {...this.props}
                      />
                </Container>
              </>

    }

};


export default (props) => {
    let new_api;
    if ((props.match.params.id === 'new') && (props.match.params.deal_id !== undefined)) {
       new_api = `/api/deals/${props.match.params.deal_id}/events/`;
    }

    const Wrapper = DetailDataLoaderWrapper(FormView, api,  resource, new_api);
    return <Wrapper {...props}/>;
}