import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router';
import { Container } from "react-bootstrap";
import { withRouter } from 'react-router';
import { connect } from 'react-redux'

import AppBar from '../components/AppBar/AppBar'
import Footer from '../components/Footer'

import {authErrors, isAuthenticated, refreshTime} from "../reducers";
import {logout} from "../actions/auth";

import SpreadSheetsListView from "./SpreadSheets/ListView"
import SpreadSheetsDetailView from "./SpreadSheets/DetailView"
import SpreadSheetsFormView from "./SpreadSheets/FormView";

import ConfirmationModal from "../components/ConfirmationModal";


export class App extends Component {

  render() {
    return <>
            <ConfirmationModal />
            <AppBar onLogout={this.props} {...this.props} />
            <div style={{marginBottom: 100, minHeight: 500}}>
                <Switch>

                    <Route exact path="/spreadsheets/" component={ SpreadSheetsListView } />
                    <Route exact path="/spreadsheets/:id/edit" component={ SpreadSheetsFormView } />
                    <Route exact path="/spreadsheets/:id/:tab" component={ SpreadSheetsDetailView } />
                    <Route exact path="/spreadsheets/:id" component={ SpreadSheetsDetailView } />

                    <Route path='*' component={ SpreadSheetsListView }/>
                </Switch>
            </div>
            <Footer {...this.props}/>
        </>
  }
}

const mapStateToProps = (state) => ({
    errors:          authErrors(state),
    isAuthenticated: isAuthenticated(state),
    refreshTime:     refreshTime(state)

})

const mapDispatchToProps = dispatch => {
  return {
        onLogout:               () => dispatch(logout())
  }
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
