import React, {Component} from "react";
import {connect} from "react-redux";
import ReactDataGrid from "react-data-grid";
import socketIOClient from "socket.io-client";
import * as actions from "../../actions/actions";
import * as actionsGrid from "../../actions/grid";
import * as status from "../../status";
import {getGridData} from "../../reducers";


class Grid extends Component {

    /**
     * Converts data into JSX table
     * @param data
     * @returns JSX table
     */

  componentDidMount() {
      const endpoint = "http://0.0.0.0:5000";
      this.socket = socketIOClient(endpoint);
      this.socket.on(actions.GRID_RECEIVE_UPDATE_SUCCESS, this.props.gridGetUpdate);
      this.props.getInitialTable(this.socket, this.props.id);
  }

  componentWillUnmount() {
    this.socket.off(actions.GRID_RECEIVE_UPDATE_SUCCESS);
  }

  onGridRowsUpdated = ({ fromRow, toRow, updated }) => {
      console.log(fromRow, toRow, updated)
      this.props.gridSendUpdate(this.socket, {tableId: this.props.id, fromRow: fromRow, toRow: toRow, updated: updated});
  };

  render() {
    console.log("GRID", this.props)
    if ((this.props.dataGrid[this.props.id] !== undefined) &&
        (this.props.dataGrid[this.props.id].status === status.STATUS_SUCCESS)) {
        const { data } = this.props.dataGrid[this.props.id];
        const columns = data.columns;
        const rows = data.rows;
        console.log("ROWW", rows);
        const rowGetter = rowNumber =>  (rows[rowNumber]) ? rows[rowNumber].cells.map(item => (item) ?item.valueString : null) : undefined;
        console.log("ROWW", rowGetter(9));

        return <ReactDataGrid
            columns={columns}
            rowGetter={rowGetter}
            rowsCount={rows.length}
            minHeight={500}
            onGridRowsUpdated={this.onGridRowsUpdated}
            enableCellSelect={true}
        />
    }
    return "Loading";
  }
}


const mapStateToProps = (state) => ({
  dataGrid: getGridData(state)
  //componentSearch:    getComponentSearch(state)

});

const mapDispatchToProps = dispatch => ({
   getInitialTable:      (socket, tableId) => dispatch(actionsGrid.getInitialTable(socket, tableId)),
   gridGetUpdate: (data) => dispatch(actionsGrid.gridGetUpdate(data)),
   gridSendUpdate: (socket, data) => dispatch(actionsGrid.gridSendUpdate(socket, data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Grid);