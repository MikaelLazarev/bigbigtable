import React, {Component} from "react";
import {Form} from "react-bootstrap";
import {connect} from "react-redux";
import {getComponentSearch} from "../reducers";
import * as actions from "../actions/dataLoader";


class ComponentSearch extends Component {

    onChange = (event) => {
        event.preventDefault()
        const value = event.target.value.toLowerCase();
        console.log("Component Search" + value)
        this.props.searchComponent(this.props.resource, value);
    }

    render() {
        return (
            <Form.Group>
                <Form.Control
                    name = {"search"}
                    value={this.props.componentSearch[this.props.resource]}
                    onChange = {event => this.onChange(event)}
                  />
            </Form.Group>
            );
    }
}


const mapStateToProps = (state) => ({
     componentSearch:    getComponentSearch(state)

});

const mapDispatchToProps = dispatch => ({
     searchComponent: (resource, query) => dispatch(actions.searchComponent(resource, query)),

});

export default connect(mapStateToProps, mapDispatchToProps)(ComponentSearch);