import React from "react";
import {Link} from "react-router-dom";
import {Button} from "react-bootstrap"

const ButtonLink = (props) => <Link to={props.to}>
                                <Button size={"sm"} style={{marginRight: 10}}>{props.title}</Button>
                              </Link>;

export default ButtonLink;