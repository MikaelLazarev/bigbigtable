 import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { AsyncTypeahead } from "react-bootstrap-typeahead";
import { getSearchItems } from "../../reducers";
import { globalSearch } from "../../actions/search";
import {connect} from "react-redux";


export class AppSearch extends Component {

    state = {
        isRedirect: false,
        redirectURL: null
    }

    onChange = (selected) => {
        const selectedItem = selected[0];
        this.setState({
            isRedirect: true,
            redirectURL: selectedItem.url
        })

    }

    render() {

        console.log(this.props.result || "No results yet")

        if (this.state.isRedirect) {
            const url = this.state.redirectURL;
            this.setState({
            isRedirect: false,
            redirectURL: null
        })
        return <Redirect to={url} />
        }

        return <AsyncTypeahead
          isLoading={this.props.result.isLoading}
          onSearch={ (query) => this.props.onSearch(query)}
          options={this.props.result.options}
          align={'right'}
          onChange={(selected) => this.onChange(selected)}
        />
    }
}


const mapStateToProps = (state) => ({
    result:     getSearchItems(state),

});

const mapDispatchToProps = dispatch => {
  return {
        onSearch: (query) => dispatch(globalSearch(query))
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(AppSearch);

/*
query => {
            this.setState({isLoading: true});
            fetch(getFullAPIAddress + 'search/?q=${query}')
              .then(resp => resp.json())
              .then(json => this.setState({
                isLoading: false,
                options: json.items,
              }));
          }




 */