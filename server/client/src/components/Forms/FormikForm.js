import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Redirect} from "react-router";
import {Button, Form} from "react-bootstrap";

import {Formik} from 'formik/dist/index';
import * as yup from 'yup';

import {getHashUpdates} from "../../reducers";
import * as actions from "../../actions/dataLoader";
import * as status from "../../status";

import AutoCompleteField from './AutoCompleteField'
import InputField from "./InputField";
import CheckBoxField from "./CheckBoxField";
import SelectField from "./SelectField";


class FormikForm extends Component {

    state = {
        status: status.STATUS_ACTIVE,
        hashSent: 0,
        redirect: undefined
    }

    onSubmit = (values) => {

        console.log("FFF-FORM", values);

        const hashSent = 'ForMIK' +  this.props.resource + this.props.id + Date();
        this.setState({status: status.STATUS_LOADING, hashSent: hashSent})
        this.props.updateDetail(this.props.api, this.props.resource, this.props.id, values, hashSent);
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        const hash = this.state.hashSent;

        if (hash === 0) return;
        if ((this.props.hashUpdates[hash] !== undefined) && (this.state.status === status.STATUS_LOADING)) {
            this.setState({status: status.STATUS_SUCCESS, hashSent: 0});
            // If form has on SuccessLink we have to redirect there
            if (this.props.onSuccessLink) { this.setState({redirect: true }) }

            // otherwise we check possibility to update current view
            if (typeof this.props.updateData === "function") {
                this.props.updateData();
            }

        }
        return
    }

    render() {

        console.log("[FOFORM]: FormikForm Component: Props: ", this.props);
        if (this.state.redirect) return <Redirect to={this.props.onSuccessLink}/>;

        const schemaPrep = {};
        Object.entries(this.props.fieldList).map(x => schemaPrep[x[0]] = x[1].validation);

        const schema = yup.object({...schemaPrep})
        return (
            <Formik
              validationSchema = {schema}
              onSubmit = { this.onSubmit }
              initialValues = {{ ...this.props.initialValues }}

              render={ props => {
                  const fields = Object.entries(this.props.fieldList).map(field => {
                       const key = field[0];
                       const value = field[1];
                       const type = field[1].type || "text";
                       const inputProps = {
                           name: key,
                           type: type,
                           title: value.title,
                           sm: value.sm || 12,
                           key,
                           ...props,
                           ...field[1]
                       }

                       switch(type){
                           case 'text':
                           case 'password':
                               return <InputField {...inputProps } />

                           case 'select':
                               return <SelectField {...inputProps } />

                           case 'textarea':
                               return <InputField {...inputProps } as={'textarea'}/>

                           case 'autocomplete':
                               return <AutoCompleteField {...inputProps}/>

                           case 'checkbox':
                               return <CheckBoxField {...inputProps}/>

                       }

                      });

                  return <Form noValidate onSubmit={props.handleSubmit}>
                      <Form.Row>
                          { fields }
                      </Form.Row>
                      <Button type="submit">Save</Button>
                  </Form>
              }
              }
            />

    );
        }



};

FormikForm.propTypes = {

    api: PropTypes.string.isRequired,
    resource: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
    hashUpdates:          getHashUpdates(state),

});

const mapDispatchToProps = dispatch => ({
      updateDetail:      (api, resource, id, file, hashSent) => dispatch(actions.updateDataLoaderDetail(api, resource, id, file, hashSent)),

});

export default  connect(mapStateToProps, mapDispatchToProps)(FormikForm)

