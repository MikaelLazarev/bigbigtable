import React, {Component} from "react";


const TableSubHeader = (props) => (

    <tr>
        <td colSpan={props.colSpan}n align={props.align}>
            <strong>
                <h5 style={{marginBottom: 0}}>{props.header}</h5>
            </strong>
        </td>
    </tr>
);

export default TableSubHeader