import React from "react";
import {Card} from "react-bootstrap"


export const WindowWidget = (props ) => (

    <Card style={{backgroundColor: "#FAFBFC", minHeight: props.minHeight}}>
        <Card.Header style={{paddingTop: 9, paddingBottom: 9}}>
            <table width="100%">
                <tbody>
                <tr valign="center">
                    <td>
                        <h5 style={{margin: "0 0 0 0"}}>{props.title}</h5>
                    </td>
                    <td align="right">
                        {props.rightToolbar}
                    </td>
                </tr>
                </tbody>
            </table>
        </Card.Header>
        <Card.Body style={{backgroundColor: "#FFFFFF"}}>
            {props.children}
        </Card.Body>

    </Card>);

export default WindowWidget;