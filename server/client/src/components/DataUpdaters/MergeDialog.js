import React, { Component } from "react";
import { connect } from "react-redux"
import { MdCallMerge } from "react-icons/md/index";
import * as actions from "../../actions/dataLoader";
import UpdateWithConfirmation from "./UpdateWithConfirmation"


class MergeDialog extends Component {

  render() {
    console.log("FOFOFO", this.props)
    return (
      <>
        <UpdateWithConfirmation
            title={"Подтвердите объединение"}
            body={`Вы действительно собираетесь объединить запись ${this.props.id} с ${this.props.alias_id} ? Восстановление будет невозможно!`}
            buttonComponent={MdCallMerge}
            actionName={"Объединить"}
            onAction={(hash) => this.props.mergeItem(this.props.api, this.props.resource, this.props.id, this.props.alias_id, hash)}
            hashPrefix={'MERGE'}
            {...this.props}
        />
      </>
    );
  }
}


const mapDispatchToProps = dispatch => ({
     mergeItem: (api, resource, id, alias_id, hashSent) => dispatch(actions.mergeDetailUpdateDetail(api, resource, id, alias_id, hashSent)),
});


export default connect(null, mapDispatchToProps)(MergeDialog);