import React from "react";
import PropTypes from "prop-types";
import { MdDelete } from "react-icons/md/index";
import {connect} from "react-redux"
import * as actions from "../../actions/modals";
import * as actionsD from "../../actions/dataLoader";


function DeleteDialog(props) {

    const confirmationDialogSettings = {
        title: "Подтвердите удаление",
        body: `Вы действительно собираетесь удалить запись ${props.id} ? Восстановление будет невозможно!`,
        buttonActionName: "Удалить",
        hashPrefix: 'DELETE',
        onAction: (hash) => props.deleteItem(props.api, props.resource, props.id, hash),
        updateData: props.updateData
    }

    return <MdDelete onClick={() => props.showConfirmation(confirmationDialogSettings) } />
}

DeleteDialog.propTypes = {

    api: PropTypes.string.isRequired,
    resource: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    showConfirmation: PropTypes.func.isRequired,
    updateData: PropTypes.func
}


const mapDispatchToProps = dispatch => ({
     showConfirmation: (data) => dispatch(actions.showConfirmation(data)),
     deleteItem: (api, resource, id, hashSent) => dispatch(actionsD.deleteDetailUpdateParent(api, resource, id, hashSent)),
});

export default connect(null, mapDispatchToProps)(DeleteDialog);