import { RSAA } from 'redux-api-middleware';
import { getFullAPIAddress } from '../utils/api'
import { withAuth } from '../reducers';
import * as actions from './actions';


export const geTypeaheadOptionsList = (api, resource, pagination) => ({
    [RSAA]: {
        endpoint: getFullAPIAddress(api),
        method: 'GET',
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [{
                    type: actions.TYPEAHEAD_LIST_REQUEST,
                    meta: { resource: resource, page: pagination }
                },
                {
                    type: actions.TYPEAHEAD_LIST_SUCCESS,
                    meta: { resource: resource, page: pagination }
                },
                {
                    type: actions.TYPEAHEAD_LIST_FAILURE,
                    meta: { resource: resource, page: pagination }
                }]
    }
});
