import { RSAA } from 'redux-api-middleware';
import { getFullAPIAddress } from '../utils/api'
import { withAuth } from '../reducers';
import * as actions from './actions';
import {getApiById} from "../utils/api";


export const getDataLoaderList = (api, resource, params) => {
    const url = getFullAPIAddress(api, params);

    console.log("QQQ5", params)
    return {
        [RSAA]: {
            endpoint: url ,
            method: 'GET',
            headers: withAuth({ 'Content-Type': 'application/json' }),
            types: [{
                    type: actions.DATA_LOADER_LIST_REQUEST,
                    meta: { resource: resource, query: params.query }
                },
            {
                type: actions.DATA_LOADER_LIST_SUCCESS,
                meta: { resource: resource, query: params.query }
            },
            {
                type: actions.DATA_LOADER_LIST_FAILURE,
                meta: { resource: resource, query: params.query }
            }]
    }
    }
};

export const updateDataLoaderList = (api, resource, params) => {
    return {
        [RSAA]: {
            endpoint: getFullAPIAddress(api, params),
            method: 'GET',
            headers: withAuth({ 'Content-Type': 'application/json' }),
            types: [{
                type: actions.DATA_LOADER_LIST_UPDATE_REQUEST,
                meta: { resource: resource, query: params.query }
            },
                {
                    type: actions.DATA_LOADER_LIST_SUCCESS,
                    meta: { resource: resource, query: params.query }
                },
                {
                    type: actions.DATA_LOADER_LIST_FAILURE,
                    meta: { resource: resource, query: params.query }
                }]
        }
    }
};

export const getDataLoaderDetail = (api, resource, id, update, params) => {

    //let add_params = (params == undefined) ? "/" : "/" + params;

    const updateRequest = (update) ? actions.DATA_LOADER_DETAIL_UPDATE : actions.DATA_LOADER_DETAIL_REQUEST;

    //console.log("ADD", add_params)
    return {
        [RSAA]: {
            endpoint: getApiById(api, id, params), // add_params),
            method: 'GET',
            headers: withAuth({ 'Content-Type': 'application/json' }),
            types: [{
                        type: updateRequest,
                        meta: { resource: resource, id: id, query: params.query },

                    },
                    {
                        type: actions.DATA_LOADER_DETAIL_SUCCESS,
                        meta: { resource: resource, id: id, query: params.query }
                    },
                    {
                        type: actions.DATA_LOADER_DETAIL_FAILURE,
                        meta: { resource: resource, id: id, query: params.query }
                    }]
        }
}};

export const updateDataLoaderDetail = (api, resource, id, data, hashSent) => {


    console.log("[APC]: Update Data Loader Detail", api, resource, id, data, hashSent);

    if ((api === undefined) || (resource === undefined) || (id === undefined)) {
        throw "Error in updateDataLoaderDetail, wrong parameters!\napi:" + api + "\nresource: " + resource + "\nid: " + id
    }

    let headers = { 'Content-Type': 'application/x-www-form-urlencoded' }
    if (hashSent === undefined) hashSent = 0;

    // If data is not formData we change Content-Type and JSONify our data
    if (!(data instanceof FormData)) {
        headers = { 'Content-Type': 'application/json' }
        data = JSON.stringify(data);
    };

    const method = (id === 'new') ? 'POST': 'PUT';
    api = (id !== 'new')? getApiById(api, id) : getApiById(api);

    console.log("DATA SENT:", data);

    return {
            [RSAA]: {
            endpoint: api,
                method: method,
                headers: withAuth(headers),
                body: data,
                types: [{
                    type: actions.DATA_LOADER_UPLOAD_REQUEST,
                    meta: { resource: resource, id: id, hash: hashSent }
                },
                {
                    type: actions.DATA_LOADER_DETAIL_SUCCESS,
                    meta: { resource: resource, id: id, hash: hashSent }
                },
                {
                    type: actions.DATA_LOADER_UPLOAD_FAILURE,
                    meta: { resource: resource, id: id, hash: hashSent }
                }]
          }
    }
};

export const deleteDetailUpdateParent = (api, resource, id, hashSent) => {

    return {
            [RSAA]: {
            endpoint: getFullAPIAddress(api + id + "/"),
                method: 'DELETE',
                headers: withAuth(),
                types: [{
                    type: actions.DATA_LOADER_DELETE_REQUEST,
                    meta: { resource: resource, id: id, hash: hashSent }
                },
                {
                    type: actions.DATA_LOADER_DELETE_SUCCESS,
                    meta: { resource: resource, id: id, hash: hashSent }
                },
                {
                    type: actions.DATA_LOADER_DETAIL_FAILURE,
                    meta: { resource: resource, id: id, hash: hashSent }
                }]
          }
    }
};


export const mergeDetailUpdateDetail = (api, resource, id, alias_id, hashSent) => {

    const params={};
    params.alias = alias_id;

    return {
            [RSAA]: {
                endpoint: getApiById(api + ":id/merge/", id, params),
                method: 'GET',
                headers: withAuth(),
                types: [{
                    type: actions.DATA_LOADER_DETAIL_UPDATE,
                    meta: { resource: resource, id: id, hash: hashSent }
                },
                {
                    type: actions.DATA_LOADER_DETAIL_SUCCESS,
                    meta: { resource: resource, id: id, hash: hashSent }
                },
                {
                    type: actions.DATA_LOADER_DETAIL_FAILURE,
                    meta: { resource: resource, id: id, hash: hashSent }
                }]
          }
    }
};

export const searchComponent = (resource, query) => (
    {
        type: actions.SEARCH_COMPONENT,
        resource: resource,
        query: query
    }
)
