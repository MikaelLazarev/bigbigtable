import { RSAA } from 'redux-api-middleware';
import { getFullAPIAddress } from '../utils/api'
import * as actions from './actions'


export const login = (username, password) => ({
    [RSAA]: {
        endpoint: getFullAPIAddress('/auth/token/obtain/'),
        method: 'POST',
        body: JSON.stringify({username, password}),
        headers: {'Content-Type': 'application/json'},
        types: [actions.LOGIN_REQUEST, actions.LOGIN_SUCCESS, actions.LOGIN_FAILURE]
    }
});


export const oauthAuthenticate = (provider, code) => ({
    [RSAA]: {
        endpoint: getFullAPIAddress( '/auth/google/done/'),
        method: 'POST',
        body: JSON.stringify({provider: provider, code: code}),
        headers: { 'Content-Type': 'application/json' },
        types: [actions.LOGIN_REQUEST, actions.LOGIN_SUCCESS, actions.LOGIN_FAILURE]
    }
});


export const signup = (email, password) => ({
    [RSAA]: {
        endpoint: getFullAPIAddress( '/auth/signup/' ),
        method: 'POST',
        body: JSON.stringify({email, password}),
        headers: { 'Content-Type': 'application/json' },
        types: [
            actions.SIGNUP_REQUEST, actions.SIGNUP_SUCCESS, actions.SIGNUP_FAILURE
        ]
      }
});


export const refreshAccessToken = (token) => ({
    [RSAA]: {
        endpoint: getFullAPIAddress( '/auth/token/refresh/' ),
        method: 'POST',
        body: JSON.stringify({refresh: token}),
        headers: { 'Content-Type': 'application/json' },
        types: [
          actions.TOKEN_REQUEST, actions.TOKEN_RECEIVED, actions.TOKEN_FAILURE
        ]
    }
});


export const logout = () => ({
    type: actions.LOGOUT
   
});




