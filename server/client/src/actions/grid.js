import * as actions from './actions';

export const getInitialTable = (socket, tableId) => {
    socket.emit("initTable", {tableId: tableId});
    return {
        type: actions.GRID_SEND_INITIAL_REQUEST,
        meta: {tableId: tableId}
    }
}

export const gridGetUpdate = (data) => ({
    type: actions.GRID_RECEIVE_UPDATE_SUCCESS,
    payload: data
});

export const gridSendUpdate = (socket, data) => {
    console.log("[ACTION]: GRID SEND UPDATE", data);
    socket.emit("updateTable", data);
    return {
        type: actions.GRID_SEND_UPDATE_REQUEST
    }
};