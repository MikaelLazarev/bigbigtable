import { RSAA } from 'redux-api-middleware';

import * as actions from "./actions";
import {getApiById} from "../utils/api";
import {withAuth} from "../reducers";


export const createForm = (formId, api) => {

    return {
        [RSAA]: {
            endpoint: getApiById(api),
            method: 'GET',
            headers: withAuth({ 'Content-Type': 'application/json' }),
            types: [{
                        type: actions.FORMS_NEW_REQUEST,
                        meta: { formId: formId },

                    },
                    {
                        type: actions.FORMS_NEW_SUCCESS,
                        meta: { formId: formId }
                    },
                    {
                        type: actions.FORMS_HIDE,
                        meta: { formId: formId }
                    }]
        }
}};

export const showForm = (formId, data) => (
    {
        type: actions.FORMS_SHOW,
        formId: formId,
        data: data
    }
);

export const hideForm = (formId) => (
    {
        type: actions.FORMS_HIDE,
        formId: formId,
    }
)

export const showConfirmation = (data) => (
    {
        type: actions.CONFIRMATION_SHOW,
        data: data
    }
);

export const hideConfirmation = () => (
    {
        type: actions.CONFIRMATION_HIDE,
    }
)

