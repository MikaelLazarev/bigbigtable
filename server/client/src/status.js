export const BACKEND_ADDR = (process.env.NODE_ENV === 'development') ? 'http://localhost:8000' : 'https://argumento-stage.herokuapp.com/';


export const STATUS_UPDATE_NEEDED  = '@@status/UPDATE_NEEDED'

export const STATUS_ACTIVE  = '@@status/ACTIVE'
export const STATUS_LOADING = '@@status/LOADING'
export const STATUS_UPDATING = '@@status/UPDATING'
export const STATUS_SUCCESS = '@@status/SUCCESS'
export const STATUS_FAILURE = '@@status/FAILURE'

