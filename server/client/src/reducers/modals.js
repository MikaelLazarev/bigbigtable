import { updateState } from "../utils/updateState";
import * as actions from '../actions/actions'


const initialState = {
  modals: {},
  confirmation: {},
};


export default (state=initialState, action) => {

  console.log("REDUCER STATE", state);
  console.log("REDUCER ACTION", action);

  let updateForms = state.modals, updateConfirmation=state.confirmation;

  switch (action.type) {

    // ====== FORMS ===============
    case actions.FORMS_SHOW:
        updateForms[action.formId] = {show: true, data: action.data};
        return updateState(state, {...state, modals: updateState(state.modals, updateForms)});

    case actions.FORMS_NEW_SUCCESS:
        const forms_data = action.payload;
        forms_data.id = "new"
        updateForms[action.meta.formId] = {show: true, data: forms_data};
        return updateState(state, {...state, modals: updateState(state.modals, updateForms)});

    case actions.FORMS_HIDE:
        const data = (state.modals['formId']) ? state.modals['formId'].data: undefined;
        updateForms[action.formId] = { show: false, data: data };
        return updateState(state, {...state, modals: updateState(state.modals, updateForms)});

    // ====== CONFIRMATION ===============
    case actions.CONFIRMATION_SHOW:
        updateConfirmation = { show: true, data:  action.data};
        return updateState(state, {...state, confirmation: updateState(state.confirmation, updateConfirmation)});

    case actions.CONFIRMATION_HIDE:
        updateConfirmation = { show: false };
        return updateState(state, {...state, confirmation: updateState(state.confirmation, updateConfirmation)});

    default:
        return state

  }
}

export const getShowForms = (state) => (state.modals);
export const getConfirmation = (state) => (state.confirmation);
