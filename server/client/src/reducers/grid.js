import { updateState } from "../utils/updateState";
import * as actions from '../actions/actions';
import * as status from '../status';


const initialState = {
  tables: {},
  search: {}
};


export default (state=initialState, action) => {

  const obj = {};
  let hash, hashUpdate;

  switch (action.type) {

    case actions.GRID_SEND_INITIAL_REQUEST:
         obj[action.meta.tableId] = {
             status: status.STATUS_LOADING,
             data: []
         }

        return updateState(state, { ...state,
                        tables: updateState(state.tables, obj) });

    case actions.GRID_SEND_UPDATE_REQUEST:
          obj[action.meta.tableId] = {
              status: status.STATUS_LOADING,
          }

          return updateState(state, { ...state,
                          tables: updateState(state.tables, obj) });


    case actions.GRID_RECEIVE_UPDATE_SUCCESS:

          console.log("GRID DETAILS", state.tables)
          console.log("GRID DETAILS", action.payload)
          // We add an info of update for current hash
          //hash = action.meta.hash || 0;
          //hashUpdate = {};
          //hashUpdate[hash] = +Date.now();

          //obj[action.payload.tableId] = state.tables[action.payload.tableId] || {};
          obj[action.payload.id] = {
              status: status.STATUS_SUCCESS,
              data: action.payload.table,
          }
          return updateState(state, {
                    ...state,
                    tables: updateState(state.tables, obj)
                });

    default:
          return state;

  }
}

export const getTableData = (state) => (state.tables);
//export const getHashUpdates = (state) => (state.updates);
//export const getComponentSearch = (state) => (state.search);
