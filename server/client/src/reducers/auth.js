import jwtDecode from 'jwt-decode'
import { updateState } from "../utils/updateState";
import * as auth from '../actions/actions'


const initialState = {
  access: undefined,
  refresh: undefined,
  errors: {},
  signup_success: false
}

export default (state=initialState, action) => {

  console.log(action)

  switch(action.type) {
    case auth.LOGIN_SUCCESS:
      return {
        ...state,
        access: {
          token: action.payload.access,
          ...jwtDecode(action.payload.access)
        },
        refresh: {
          token: action.payload.refresh,
          ...jwtDecode(action.payload.refresh)
        },
        profile: undefined,
        isVerified: undefined,
        errors: {}
    }
    case auth.TOKEN_RECEIVED:
      return updateState(state, {
                                    ...state,
                                    access: {
                                      token: action.payload.access,
                                      ...jwtDecode(action.payload.access)
                                    }
                                  })


    case auth.LOGIN_FAILURE:
    case auth.TOKEN_FAILURE:
    case auth.SIGNUP_FAILURE:

      return {
        ...state,
         access: undefined,
         refresh: undefined,
         errors: action.payload.response || {'non_field_errors': action.payload.statusText},
      }

    case auth.LOGOUT:
      return {
         ...state,
         access: undefined,
         refresh: undefined,
      }

    case auth.SIGNUP_SUCCESS:
      return {
        ...state,
        signup_success: true

      }
    default:
      return state
    }
}

export function accessToken(state) {
  if (state.access) {
    return  state.access.token
  }
}

export function isAccessTokenExpired(state) {
  if (state.access && state.access.exp) {
    return 1000 * state.access.exp - (new Date()).getTime() < 5000
  }
  return true
}

export function refreshToken(state) {
  if (state.refresh) {
    return  state.refresh.token
  }
}

export function isRefreshTokenExpired(state) {
  if (state.refresh && state.refresh.exp) {
    return 1000 * state.refresh.exp - (new Date()).getTime() < 5000
  }
  return true
}

export function isAuthenticated(state) {
  return !isRefreshTokenExpired(state)
}

export function errors(state) {
  return  state.errors
}

export const refreshTime = (state) => state

export const signupSuccess = (state) => state.signup_success
