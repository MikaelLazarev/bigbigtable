import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import auth, * as fromAuth from './auth'
import search, * as fromSearch from './search'
import dataLoader, * as fromDataLoader from './dataLoader'
import grid, * as fromGird from './grid'
import modals, * as fromModals from './modals';
import typeAhead, * as fromTypeAhead from './typeAhead'


export default (history) => combineReducers({

    router:  connectRouter(history),
    auth: auth,
    search: search,
    dataLoader: dataLoader,
    grid: grid,
    modals: modals,
    typeAhead: typeAhead

})

// Authentication
export const isAuthenticated = state => fromAuth.isAuthenticated(state.auth);
export const accessToken = state => fromAuth.accessToken(state.auth);
export const isAccessTokenExpired = state => fromAuth.isAccessTokenExpired(state.auth);
export const refreshToken = state => fromAuth.refreshToken(state.auth);
export const isRefreshTokenExpired = state => fromAuth.isRefreshTokenExpired(state.auth);
export const authErrors = state => fromAuth.errors(state.auth);
export const refreshTime = state => fromAuth.refreshTime(state.auth);
export const getSignupSuccess = state => fromAuth.signupSuccess(state.auth);

// Search
export const getSearchItems = state => fromSearch.getSearchItems(state.search);

// Grid
export const getGridData = state => fromGird.getTableData(state.grid);

// DataLoader
export const getDataList = state => fromDataLoader.getDataList(state.dataLoader);
export const getDataDetails = state => fromDataLoader.getDataDetails(state.dataLoader);
export const getHashUpdates = state => fromDataLoader.getHashUpdates(state.dataLoader);
export const getComponentSearch = state => fromDataLoader.getComponentSearch(state.dataLoader);

// Modals
export const getShowForms = state => fromModals.getShowForms(state.modals);
export const getConfirmation = state => fromModals.getConfirmation(state.modals);

// TypeAhead
export const getOptionsList = state => fromTypeAhead.getOptionsList(state.typeAhead);

export function withAuth(headers={}) {
  return (state) => ({
    ...headers,
    //'Authorization': `Bearer ${accessToken(state)}`
  })
}
