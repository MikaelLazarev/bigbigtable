# Bigtable
## Decentralised online spreadsheet editin tool

DEMO VIDEO: https://youtu.be/l5U_igJH7p8

## Problem
Online documents editing is one of the amazing things appeared last years, but using them you have to trust cloud provider (like Google or Microsoft). Cloud providers could read your documents and use this data on their needs.

## Solution
Create p2p systems which provides all opportunities of document online editing. It uses NuCypher as secure layer to exchange information between peers.

## How to install
1. Download or clone this repository
2. Run Docker compose file

Disclaimer: it perfectly works on my own computer but sometimes it could be issues with starting some containers.

## Architecture
System is consists of 5 services:

- Server (provides local server & frontend)
- Worker (NuCypher microservice. Used to encrypt / decrypt commits)
- Exchnage (Microservices which is used to broadcast encrypted commits)
- Mongo DB (Stores local data. It also used for exchange, but you could run another instance for that separately)
- Redis (user to manage queue for NuCypher microservice)

![bigbigtable.005](/uploads/c7679d31778ec3d6e5d460a93e589771/bigbigtable.005.jpeg)



## Technologies used
Python, Javascript, Mongo DB, NuCypher, Socker.io, Redis, Express

### Benefits of using NuCypher
NuCypher adds another secured layer. You could manage permissions of you documents with your team or friends.

## How it works

### 1. When you make a change, Client creates a commit and send it to server:

![bigbigtable.006](/uploads/279e00f1f204cd5e91846ed63f6f54b5/bigbigtable.006.jpeg)

### 2. Server changes database and send this commit to queue to NuCypoher microservice

![bigbigtable.007](/uploads/2a4e824f28f329ad09815572e0fcb146/bigbigtable.007.jpeg)

### 3. NuCypher microservice encrypts commit (we could add random salt for better security cause commit has small size) and sends it back to server.

![bigbigtable.008](/uploads/9153ba1b4581b1fe9e6aca61c67a390a/bigbigtable.008.jpeg)

### 4. Server sends encrypted commit to exchange

![bigbigtable.009](/uploads/a6b5f493e30a756554fe1725dd30e2fe/bigbigtable.009.jpeg)

### 5. Exchange broadcasts secured commits. Servers could subscribe for interested spreadsheeds (you should know uuid) to received commits.

![bigbigtable.010](/uploads/d538fa25a90fb2d3067de0ee07a395bc/bigbigtable.010.jpeg)

### 6. Another server receives commit from exchange

![bigbigtable.011](/uploads/071e704a046c9bf821d374a02726be69/bigbigtable.011.jpeg)

### 7. Another server sends it to queue to NuCypher microservice
 
![bigbigtable.012](/uploads/ca8d629a45aa6c5135543f9ba71d2298/bigbigtable.012.jpeg)

### 8. Nucypher microservice decrypts it and return decrypted commit to Server

![bigbigtable.013](/uploads/fd300ab652c8537289ba02e85af17349/bigbigtable.013.jpeg)

### 9. Another server update client (using socket.io)
 
![bigbigtable.014](/uploads/831612d31db4a4f9d9f23c355ea840a8/bigbigtable.014.jpeg)

## Thank you!
![bigbigtable.015](/uploads/3793b2f769f118106e753824970965f8/bigbigtable.015.jpeg)













