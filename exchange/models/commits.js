const mongoose = require('mongoose');
const uuidv4 = require('uuid/v4');
const { Schema } = mongoose;


const commitSchema = new Schema ({

    uuid: {type: String },
    commitNumber: {type: Number},
    body: {type: String},
    created: { type: Date, default: Date.now }
});

commitSchema.virtual('id').get(function(){
    return this._id;
});

// Ensure virtual fields are serialised.
commitSchema.set('toJSON', {
    virtuals: true
});

mongoose.model('Commits', commitSchema);
