const mongoose = require('mongoose');
const Commit = mongoose.model('Commits');
const { pushCommit } = require('../models/commits');



module.exports = socket => {

  socket.on('push_commit',  async(newCommit) => {
    console.log("Exchange received new commit: ", newCommit);
    const { uuid, body } = newCommit;

    const newCommitObj = Commit(newCommit);
    await newCommitObj.save();

    // DUPLICATE FOR DEMO PURPOSES ONLY
    const echoCommitObj = Commit(newCommit);
    echoCommitObj.uuid += "_ECHO";
    await echoCommitObj.save();

    console.log("New commit was writen at. Commit num is ", newCommitObj.commitNumber);
     // Send pull requests to all members
    socket.emit('pull_commit', newCommitObj);
    socket.emit('pull_commit', echoCommitObj);

  });

  socket.on('pull_batch_commit', async(request) => {
    const {uuid, fromCommit} = request;
    console.log("PULL BATCH COMMIT REQUEST: ", uuid, fromCommit);
    
    const updates = await Commit.find({uuid: uuid});
    console.log("Found entries: ", updates);

    socket.emit("pull_batch_return", {uuid, updates});

  })

  

};