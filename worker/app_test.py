#!/usr/bin/env python
import os
import json
import time
import logging
from typing import *

from crypto.reader import ChannelReader
from crypto.manager import ChannelManager
from crypto.channel import Channel

from crypto.data_pkg import EncryptedDataPackage
from crypto.test import test_crypto

test_crypto()
